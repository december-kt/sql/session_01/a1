1. List the books authored by Marjorie Green

	-- The Busy Executive's Database Guide
	-- You Can Combat Computer Stress!

2. List the books authored by Michael O'Leary

	-- Cooking with Computers
	-- (Not included in the database)

3. Write the author's of "The Busy Executive's Database Guide"

	-- Marjorie Green

4. Identify the publisher of "But Is It User Friendly?"

	-- Algodata Infosystems

5. List the books published by Algodata Infosystems

	-- The Busy Executive's Database Guide
	-- Cooking with Computers
	-- Straight Talk About Computers
	-- But Is It User Friendly?
	-- Secrets of Silicon Valley
	-- Net Etiquette 